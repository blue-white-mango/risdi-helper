# risdi-helper

This tool helps dosing 6,6ml of Evrysdi/Risdiplam.

Warning: Correct dosing is in your responsibility. Syringes might change. When using the helper, check that the piston is in the right position for 6,6ml. 