include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>

target_distance = 44.35; // v1: 44.7
excess_length = 4;
strength = 2;

piston_width = 20;
piston_height = 1.5;
syringe_width = 20;
syringe_height = 1.9;

solid_size = [max(piston_width, syringe_width)+5, target_distance+syringe_height+piston_height+2*excess_length, strength];

difference() {
  // solid
  ymove(-excess_length-piston_height)
    cuboid(size=solid_size, chamfer=strength, edges=EDGES_Z_BK, align=V_BACK);
  // slot piston
  cuboid(size=[piston_width, piston_height, strength+2], align=V_FRONT);
  // slot syringe
  ymove(target_distance)
    cuboid(size=[syringe_width, syringe_height, strength+2], align=V_BACK);
}
